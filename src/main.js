// const name='Regina';
// const url='images/'+name.toLowerCase()+'.jpg';
 const cssClass='<article class="pizzaThumbnail">';
// const html_old='<a href="'+url+'">'+'<img src="'+url+'"/><section>'+name+'</section></a>';
// const html=cssClass+html_old+'</article>';

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

//SORT 

// data.sort(function(a,b) { 
// 	//console.log(a.name - b.name)
//     return b.name.length - a.name.length;
// } );
//  data.sort((a,b)=> b.name.length - a.name.length);
//  data.sort((a,b)=> a.price_small - b.price_small);
data.sort(function(a,b){
	const {price_small: a_small,price_large: a_large}=a;
	const {price_small: b_small, price_large: b_large}=b;

	if (a_small - b_small == 0) return a_large - b_large;
	return a_small - b_small;
});

function tomatoBase(element){
	return element.base=='tomate';
}

function cheapLilFormat(element){
	return element.price_small<6.0;
}
/*
function containsII(element){
	let current=element.name;
	if (current.toLowerCase().includes('i')){
		current.replace('i',' ');
		if (current.toLowerCase().includes('i')) return true;
	}
	return false;
}
*/

const total= [''];
/*
for(let i=0; i<data.length;i++){
    const name=data[i];
    const url='images/'+name.toLowerCase()+'.jpg';
    const html_old='<a href="'+url+'">'+'<img src="'+url+'"/><section>'+name+'</section></a>';
    const html=cssClass+html_old+'</article>';
    total.push(html);
}
*/

data.filter(tomatoBase).forEach(function(pizza){
	const {name,image,price_small,price_large}=pizza;
    const href='<a href="'+image+'"><img src="'+image+'"/>';
    const section='<section><h4>'+name+'</h4>';
    const list='<ul><li>Prix petit format: '+price_small+'€</li><li>Prix grand format: '+price_large+'€</li></ul>';
    const html=cssClass+href+section+list+'</section></a></article>';
    total.push(html);
});
document.querySelector('.pageContainer').innerHTML = total;